use clox::vm::{InterpretError, VM};
use std::io::{BufRead, Read};
use std::path::{Path, PathBuf};

fn print_error(e: InterpretError) {
    match e {
        InterpretError::Compile => {
            eprintln!("Compilation error");
        }
        InterpretError::Runtime => {
            eprintln!("Runtime error");
        }
    }
}

fn repl(vm: &mut VM) {
    let mut line = String::new();
    let stdin = std::io::stdin();
    loop {
        line.clear();
        match stdin.lock().read_line(&mut line) {
            Err(e) => {
                println!("Failed to read line from stdin: {}", e);
                return;
            }
            Ok(size) => {
                if size == 0 {
                    return;
                }
            }
        }

        if !line.is_ascii() {
            eprintln!("This interpreter only supports ascii encodings");
            continue;
        }
        if let Err(e) = vm.interpret_source(line.as_str()) {
            eprint!("Failed to interpret line: ");
            print_error(e);
        }
    }
}

fn read_file(path: &Path) -> Result<String, ()> {
    match std::fs::File::open(path) {
        Ok(mut file) => {
            let mut contents = String::new();
            if let Err(error) = file.read_to_string(&mut contents) {
                eprintln!("Failed to read contents of file: {}", error);
                return Err(());
            }
            Ok(contents)
        }
        Err(error) => {
            eprintln!(
                "Error opening file {}: {}",
                path.to_str().unwrap_or("<??>"),
                error
            );
            Err(())
        }
    }
}

fn main() -> Result<(), ()> {
    let args: Vec<String> = std::env::args().collect();
    let mut vm = VM::new();
    if vm.init().is_err() {
        eprintln!("Failed to init vm");
        return Err(());
    }

    if args.len() == 1 {
        repl(&mut vm);
    } else if args.len() == 2 {
        let path = PathBuf::from(&args[1]);
        match read_file(&path) {
            Ok(contents) => {
                if !contents.is_ascii() {
                    eprintln!("This interpreter only supports ascii encodings.");
                    return Err(());
                }
                if let Err(e) = vm.interpret_source(contents.as_str()) {
                    eprint!("Failed to interpreting file {}: ", args[1]);
                    print_error(e);
                }
            }
            Err(_) => return Err(()),
        }
    } else {
        eprint!("Usage: {} [path]", args[0]);
        return Err(());
    }
    Ok(())
}
