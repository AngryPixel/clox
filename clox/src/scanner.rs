#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub(crate) enum TokenType {
    // Single Char Tokens
    LeftParen = 0,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    SemiColon,
    Slash,
    Star,

    // One or tow char tokens,
    Bang,
    BangEqual,
    Equal,
    EqualEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,

    // Literals
    Identifier,
    String,
    Number,

    // Keywords
    And,
    Class,
    Else,
    False,
    For,
    Function,
    If,
    Nil,
    Or,
    Print,
    Return,
    Super,
    This,
    True,
    Var,
    While,

    // Error and Eof
    Error,
    Eof,
}

fn is_identifier_aplha(ch: char) -> bool {
    return ch.is_ascii_alphabetic() || ch == '_';
}

#[derive(Debug, Copy, Clone)]
pub(crate) struct Token<'a> {
    pub(crate) ttype: TokenType,
    pub(crate) text: &'a [u8],
    pub(crate) offset: usize,
    pub(crate) line: usize,
}
impl<'a> Default for Token<'a> {
    fn default() -> Self {
        Self {
            ttype: TokenType::Eof,
            text: &[],
            offset: 0,
            line: 0,
        }
    }
}

#[derive(Debug)]
pub(crate) struct Scanner<'a> {
    pub(crate) text: &'a [u8],
    start: usize,
    current: usize,
    line: usize,
}

impl<'a> Scanner<'a> {
    pub(crate) fn new(text: &'a str) -> Self {
        Self {
            text: text.as_bytes(),
            start: 0,
            current: 0,
            line: 0,
        }
    }

    fn make_token(&self, tt: TokenType) -> Token<'a> {
        Token {
            ttype: tt,
            text: &self.text[self.start..self.current],
            line: self.line,
            offset: self.start,
        }
    }

    fn make_error_token(&self, err: &'static str) -> Token<'a> {
        Token {
            ttype: TokenType::Error,
            text: err.as_bytes(),
            line: self.line,
            offset: 0,
        }
    }

    fn is_eos(&self) -> bool {
        self.current >= self.text.len()
    }

    fn advance(&mut self) -> Option<char> {
        if self.is_eos() {
            return None;
        }
        let index = self.current;
        self.current += 1;
        Some(self.text[index] as char)
    }

    fn peek(&self) -> Option<char> {
        if self.is_eos() {
            return None;
        }
        Some(self.text[self.current] as char)
    }

    fn peek_next(&self) -> Option<char> {
        if self.current + 1 >= self.text.len() {
            return None;
        }
        Some(self.text[self.current + 1] as char)
    }

    fn do_match(&mut self, expected: char) -> bool {
        if self.is_eos() || expected != self.text[self.current] as char {
            return false;
        }
        self.current += 1;
        true
    }

    fn skip_white_space(&mut self) {
        loop {
            if let Some(ch) = self.peek() {
                match ch {
                    ' ' | '\t' | '\r' => {
                        self.advance().unwrap();
                    }
                    '\n' => {
                        self.line += 1;
                        self.advance().unwrap();
                    }
                    '/' => {
                        if let Some(next_ch) = self.peek_next() {
                            if next_ch == '/' {
                                // Comments go to the end of the line
                                while let Some(ch) = self.peek() {
                                    if ch != '\n' {
                                        self.advance().unwrap();
                                    } else {
                                        break;
                                    }
                                }
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                    _ => return,
                };
            } else {
                return;
            }
        }
    }

    fn scan_string(&mut self) -> Token<'a> {
        while let Some(ch) = self.peek() {
            if ch == '"' {
                break;
            }
            if ch == '\n' {
                self.line += 1;
            }
            self.advance().unwrap();
        }

        if self.is_eos() {
            return self.make_error_token("Unterminated String.");
        }

        self.advance().unwrap();
        self.make_token(TokenType::String)
    }

    fn check_keyword(
        &mut self,
        start: usize,
        length: usize,
        rest: &[u8],
        tt: TokenType,
    ) -> TokenType {
        if self.current - self.start == start + length
            && &self.text[self.start + start..self.current] == rest
        {
            return tt;
        }

        TokenType::Identifier
    }

    fn identifier_type(&mut self) -> TokenType {
        match self.text[self.start] as char {
            'a' => self.check_keyword(1, 2, "nd".as_bytes(), TokenType::And),
            'c' => self.check_keyword(1, 4, "lass".as_bytes(), TokenType::Class),
            'e' => self.check_keyword(1, 3, "lse".as_bytes(), TokenType::Else),
            'f' => {
                if self.current - self.start > 1 {
                    return match self.text[self.start + 1] as char {
                        'a' => self.check_keyword(2, 3, "lse".as_bytes(), TokenType::False),
                        'o' => self.check_keyword(2, 1, "r".as_bytes(), TokenType::For),
                        'u' => self.check_keyword(2, 1, "n".as_bytes(), TokenType::Function),
                        _ => TokenType::Identifier,
                    };
                }
                TokenType::Identifier
            }
            'i' => self.check_keyword(1, 1, "f".as_bytes(), TokenType::If),
            'n' => self.check_keyword(1, 2, "il".as_bytes(), TokenType::Nil),
            'o' => self.check_keyword(1, 1, "r".as_bytes(), TokenType::Or),
            'p' => self.check_keyword(1, 4, "rint".as_bytes(), TokenType::Print),
            'r' => self.check_keyword(1, 5, "eturn".as_bytes(), TokenType::Return),
            's' => self.check_keyword(1, 4, "uper".as_bytes(), TokenType::Super),
            't' => {
                if self.current - self.start > 1 {
                    return match self.text[self.start + 1] as char {
                        'h' => self.check_keyword(2, 2, "is".as_bytes(), TokenType::This),
                        'r' => self.check_keyword(2, 2, "ue".as_bytes(), TokenType::True),
                        _ => TokenType::Identifier,
                    };
                }
                TokenType::Identifier
            }
            'v' => self.check_keyword(1, 2, "ar".as_bytes(), TokenType::Var),
            'w' => self.check_keyword(1, 4, "hile".as_bytes(), TokenType::While),
            _ => TokenType::Identifier,
        }
    }

    fn scan_identifier(&mut self) -> Token<'a> {
        while let Some(ch) = self.peek() {
            if is_identifier_aplha(ch) || ch.is_ascii_digit() {
                self.advance().unwrap();
            } else {
                break;
            }
        }
        let tt = self.identifier_type();
        self.make_token(tt)
    }

    fn scan_number(&mut self) -> Token<'a> {
        while let Some(ch) = self.peek() {
            if ch.is_ascii_digit() {
                self.advance().unwrap();
            } else {
                break;
            }
        }

        // Look for fractional part
        if let Some(ch) = self.peek() {
            if ch == '.' {
                self.advance().unwrap();
                while let Some(ch) = self.peek() {
                    if ch.is_ascii_digit() {
                        self.advance().unwrap();
                    } else {
                        break;
                    }
                }
            }
        }
        self.make_token(TokenType::Number)
    }

    pub(crate) fn scan_token(&mut self) -> Token<'a> {
        self.skip_white_space();

        self.start = self.current;

        let ch = match self.advance() {
            Some(c) => c,
            None => return self.make_token(TokenType::Eof),
        };

        if is_identifier_aplha(ch) {
            return self.scan_identifier();
        }

        if ch.is_ascii_digit() {
            return self.scan_number();
        }
        return match ch {
            '(' => self.make_token(TokenType::LeftParen),
            ')' => self.make_token(TokenType::RightParen),
            '{' => self.make_token(TokenType::LeftBrace),
            '}' => self.make_token(TokenType::RightBrace),
            ';' => self.make_token(TokenType::SemiColon),
            ',' => self.make_token(TokenType::Comma),
            '.' => self.make_token(TokenType::Dot),
            '-' => self.make_token(TokenType::Minus),
            '+' => self.make_token(TokenType::Plus),
            '*' => self.make_token(TokenType::Star),
            '/' => self.make_token(TokenType::Slash),

            '!' => {
                let tt = if self.do_match('=') {
                    TokenType::BangEqual
                } else {
                    TokenType::Bang
                };
                self.make_token(tt)
            }
            '=' => {
                let tt = if self.do_match('=') {
                    TokenType::EqualEqual
                } else {
                    TokenType::Equal
                };
                self.make_token(tt)
            }
            '<' => {
                let tt = if self.do_match('=') {
                    TokenType::LessEqual
                } else {
                    TokenType::Less
                };
                self.make_token(tt)
            }
            '>' => {
                let tt = if self.do_match('=') {
                    TokenType::GreaterEqual
                } else {
                    TokenType::Greater
                };
                self.make_token(tt)
            }
            '"' => self.scan_string(),

            _ => self.make_error_token("Unexpected character."),
        };
    }
}
