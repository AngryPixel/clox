use crate::common::*;
use crate::compiler::{Compiler, FunctionType, Parser};
#[cfg(feature = "trace_execution")]
use crate::disassemble::disassemble_instruction;
use crate::scanner::Scanner;
use crate::value::{
    print_value, Closure, Function, GCObject, GCObjectPtr, GCObjectPtrView, InternedStr,
    InternedStrData, InternedStrKey, NativeFn, NativeFunction, ObjUpValue, ObjValue,
    ObjectAllocator, Value, CONSTANT_LONG_MAX_INDEX_VALUE,
};
use num_traits::FromPrimitive;
use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt::Debug;
use std::io::Write;
use std::ops::{Deref, DerefMut};
use std::ptr::NonNull;
use std::rc::Rc;
use std::time::SystemTime;

#[derive(Debug, Copy, Clone)]
pub enum InterpretError {
    Compile,
    Runtime,
}

pub type InterpretResult<T> = Result<T, InterpretError>;
const INITIAL_STACK_CAPACITY: usize = 256;
const FRAMES_MAX: usize = 64;

#[doc(hidden)]
/// Help to generate arithmetic operators for the vm
macro_rules! impl_vm_arithmetic_op {
    ($ident:ident, $op:tt, $result:ident) => {
        // Pops are backwards due to stack order
        let vals = ($ident.peek_value(0), $ident.peek_value(1));
        match vals {
            (Some(Value::Number(b)), Some(Value::Number(a))) => {
                $ident.pop_value();
                $ident.pop_value();
                $ident.push_value(Value::$result(a $op b));
            },
            _ => {
                $ident.runtime_error("Operands must be numbers");
                return Err(InterpretError::Runtime);
            }
        }
    }
}

#[derive(Debug)]
struct CallFrame {
    closure: GCObjectPtrView<Closure>,
    ip: usize,
    // index into the vm value stack for the first slot this call frame can use
    slots: usize,
}

impl CallFrame {
    fn new(closure: GCObjectPtrView<Closure>) -> Self {
        Self {
            closure,
            ip: 0,
            slots: 0,
        }
    }
}

fn clock_native(_: &mut [Value]) -> Value {
    Value::from(
        std::time::SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs_f64(),
    )
}

#[derive(Debug)]
pub struct VM {
    stack: Vec<Value>,
    globals: HashMap<InternedStrKey, Value>,
    frames: Vec<CallFrame>,
    open_upvalues: Vec<GCObjectPtrView<ObjUpValue>>,
    gray_stack: Vec<GCObjectPtr>,
    objects: Option<NonNull<GCObject>>,
    string_table: HashMap<InternedStrKey, InternedStrData>,
    is_compiling: bool,
    bytes_allocated: usize,
    next_gc: usize,
}

impl VM {
    pub fn new() -> Self {
        Self {
            stack: Vec::with_capacity(INITIAL_STACK_CAPACITY),
            // limited to 255 atm
            globals: HashMap::with_capacity(u8::MAX as usize),
            frames: Vec::with_capacity(FRAMES_MAX),
            open_upvalues: Vec::with_capacity(8),
            gray_stack: Vec::with_capacity(16),
            objects: None,
            string_table: HashMap::with_capacity(32),
            is_compiling: false,
            bytes_allocated: 0,
            next_gc: 1024,
        }
    }

    pub fn init(&mut self) -> Result<(), ()> {
        self.define_native("clock", clock_native);
        return Ok(());
    }

    pub fn shutdown(&mut self) {
        self.free_objects();
    }

    pub fn interpret_source(&mut self, text: &str) -> InterpretResult<()> {
        self.is_compiling = true;
        let mut parser = Parser::new();
        let mut scanner = Scanner::new(text);
        let mut compiler = Compiler::new(
            None,
            &mut scanner,
            &mut parser,
            FunctionType::Script,
            Rc::new(RefCell::new(self)),
        );
        if let Ok(function) = compiler.compile() {
            self.push_value(Value::Object(function.clone()));
            let closure = self.allocate_closure(function);
            self.pop_value();
            self.push_value(Value::Object(closure.clone()));
            self.call(closure, 0);
            self.run()
        } else {
            Err(InterpretError::Compile)
        }
    }

    fn runtime_error(&self, str: &str) {
        let _ = std::io::stderr().lock().write(str.as_bytes());
        eprintln!();
        if self.frames.len() != 0 {
            for frame in self.frames.iter().rev() {
                let function = &frame.closure.function;
                eprint!("[Line {:04}] in ", function.chunk.get_lines()[frame.ip]);
                if let Some(x) = function.name {
                    x.with_string(|str| {
                        eprintln!("{}()", str.as_str());
                    });
                } else {
                    eprintln!("script");
                }
            }
        }
    }

    pub fn define_native(&mut self, name: &str, func: NativeFn) {
        //Note: pushing and popping related to GC (later chapter)
        let str_key = self.allocate_string(name);
        self.push_value(Value::Object(str_key));
        let fn_native = self.allocate_native_function(func);
        self.push_value(Value::Object(fn_native));
        let intern_key = InternedStrKey {
            ptr: str_key.with_string(|x| NonNull::from(x.as_str())),
            gcobj: Some(str_key.clone()),
        };
        self.globals.insert(intern_key, Value::Object(fn_native));
        self.pop_value();
        self.pop_value();
    }

    fn current_call_frame_mut(&mut self) -> &mut CallFrame {
        self.frames.last_mut().unwrap()
    }

    fn current_call_frame(&self) -> &CallFrame {
        self.frames.last().unwrap()
    }

    fn read_byte(&mut self) -> u8 {
        debug_assert!(
            self.current_call_frame_mut().ip
                < self
                    .current_call_frame_mut()
                    .closure
                    .function
                    .chunk
                    .get_opcodes()
                    .len()
        );
        let index = self.current_call_frame_mut().ip;
        self.current_call_frame_mut().ip += 1;
        self.current_call_frame_mut()
            .closure
            .function
            .chunk
            .get_opcodes()[index]
    }

    fn read_short(&mut self) -> u16 {
        debug_assert!(
            self.current_call_frame_mut().ip + 2
                < self
                    .current_call_frame_mut()
                    .closure
                    .function
                    .chunk
                    .get_opcodes()
                    .len()
        );
        self.current_call_frame_mut().ip += 2;
        (self
            .current_call_frame()
            .closure
            .function
            .chunk
            .get_opcodes()[self.current_call_frame().ip - 2] as u16)
            << 8
            | self
                .current_call_frame()
                .closure
                .function
                .chunk
                .get_opcodes()[self.current_call_frame().ip - 1] as u16
    }

    fn read_constant(&mut self) -> Value {
        let constant_index = self.read_byte();
        debug_assert!(
            (constant_index as usize)
                < self
                    .current_call_frame()
                    .closure
                    .function
                    .chunk
                    .get_constants()
                    .len()
        );
        self.current_call_frame()
            .closure
            .function
            .chunk
            .get_constants()[constant_index as usize]
            .clone()
    }

    fn read_constant_long(&mut self) -> Value {
        let bytes: [u8; 4] = [self.read_byte(), self.read_byte(), self.read_byte(), 0];
        let constant_index = u32::from_ne_bytes(bytes);
        debug_assert!(constant_index <= CONSTANT_LONG_MAX_INDEX_VALUE);
        debug_assert!(
            (constant_index as usize)
                < self
                    .current_call_frame()
                    .closure
                    .function
                    .chunk
                    .get_constants()
                    .len()
        );
        self.current_call_frame()
            .closure
            .function
            .chunk
            .get_constants()[constant_index as usize]
            .clone()
    }

    fn push_value(&mut self, value: Value) {
        self.stack.push(value)
    }

    fn pop_value(&mut self) -> Option<Value> {
        self.stack.pop()
    }

    fn peek_value(&mut self, index: usize) -> Option<Value> {
        if index >= self.stack.len() {
            return None;
        }
        let len = self.stack.len();
        Some(self.stack[len - 1 - index].clone())
    }

    fn pop_until_size(&mut self, size: usize) {
        while self.stack.len() != size {
            self.pop_value();
        }
    }

    fn call(&mut self, cl: GCObjectPtr, arg_count: u8) -> bool {
        if let ObjValue::Closure(closure) = &cl.deref().object {
            if closure.function.arity != arg_count as i32 {
                self.runtime_error(&format!(
                    "Expected {} arguments but got {}",
                    closure.function.arity, arg_count
                ));
                return false;
            }
            self.frames
                .push(CallFrame::new(cl.to_closure_view().unwrap()));
            let frame = self.frames.last_mut().unwrap();
            frame.ip = 0;
            frame.slots = self.stack.len() - arg_count as usize - 1;
            true
        } else {
            self.runtime_error(&format!("Expected closure object"));
            false
        }
    }

    fn call_value(&mut self, value: Value, arg_count: u8) -> bool {
        if let Value::Object(object) = value {
            match &object.object {
                ObjValue::Closure(_) => self.call(object, arg_count),
                ObjValue::NativeFunction(nf) => {
                    let stack_len = self.stack.len();
                    let result =
                        (nf.func)(&mut self.stack[stack_len - arg_count as usize..stack_len]);
                    self.pop_until_size(stack_len - arg_count as usize - 1);
                    self.push_value(result);
                    true
                }
                _ => {
                    self.runtime_error("Can only call functions and classes");
                    false
                }
            }
        } else {
            self.runtime_error("Can only call functions and classes");
            false
        }
    }

    fn capture_upvalue(&mut self, stack_index: usize) -> GCObjectPtrView<ObjUpValue> {
        let upvalues_len = self.open_upvalues.len();
        let mut i = 0usize;
        while i < upvalues_len {
            if let ObjUpValue::Ptr(index) = self.open_upvalues[i].deref() {
                if *index <= stack_index {
                    break;
                } else {
                    i += 1;
                }
            } else {
                panic!("Should always be a valid stack index");
            }
        }
        if i == stack_index && !self.open_upvalues.is_empty() {
            return self.open_upvalues[i].clone();
        }

        let obj = self
            .allocate_upvalue(ObjUpValue::new(stack_index))
            .to_upvalue_view()
            .unwrap();
        self.open_upvalues.insert(i, obj);
        self.open_upvalues[i].clone()
    }

    fn close_upvalues(&mut self, stack_index: usize) {
        let mut index = 0usize;
        while index < self.open_upvalues.len() {
            let upvalue_index = match *self.open_upvalues[index].deref() {
                ObjUpValue::Ptr(i) => i,
                _ => panic!("Expect stack location"),
            };
            if upvalue_index >= stack_index {
                *self.open_upvalues[index].deref_mut() =
                    ObjUpValue::Closed(self.stack[upvalue_index].clone());
                index += 1;
            } else {
                break;
            }
        }
        for _ in 0..index {
            self.open_upvalues.remove(0);
        }
    }

    pub fn debug_print_stack(&self) {
        print!("        ");
        for v in &self.stack {
            print!("[");
            print_value(v);
            print!("]");
        }
        println!();
    }

    fn run(&mut self) -> InterpretResult<()> {
        self.is_compiling = false;
        loop {
            #[cfg(feature = "trace_execution")]
            disassemble_instruction(
                &self.current_function().chunk,
                self.current_call_frame().ip,
                &self.table,
                &self.functions,
            );
            #[cfg(feature = "trace_execution")]
            self.debug_print_stack();

            let byte = self.read_byte();
            let instruction: OpCode = FromPrimitive::from_u8(byte).unwrap();
            match instruction {
                OpCode::Return => {
                    let value = self.pop_value();
                    self.frames.pop();
                    if self.frames.is_empty() {
                        self.pop_value();
                        return Ok(());
                    }

                    // TODO: Update stack implementation to have separate offset
                    self.pop_until_size(self.frames.last().unwrap().slots);

                    if let Some(v) = value {
                        self.push_value(v);
                    } else {
                        debug_assert!(false, "There must be a valid value on the stack");
                        self.push_value(Value::Nil);
                    }
                }
                OpCode::Constant => {
                    let constant = self.read_constant();
                    self.push_value(constant);
                }
                OpCode::ConstantLong => {
                    let constant = self.read_constant_long();
                    self.push_value(constant);
                }
                OpCode::Negate => {
                    match self.peek_value(0) {
                        Some(Value::Number(c)) => {
                            let _ = self.pop_value();
                            self.push_value(Value::Number(-c));
                        }
                        _ => {
                            self.runtime_error("Operand must be a number");
                            return Err(InterpretError::Runtime);
                        }
                    };
                }
                OpCode::Add => {
                    let vals = (self.peek_value(0), self.peek_value(1));
                    match vals {
                        (Some(Value::Number(b)), Some(Value::Number(a))) => {
                            self.pop_value();
                            self.pop_value();
                            self.push_value(Value::Number(a + b));
                        }
                        (Some(Value::Object(b)), Some(Value::Object(a))) => {
                            if b.is_string() && a.is_string() {
                                let mut str = String::new();
                                a.with_string(|s| str.push_str(s.as_str()));
                                b.with_string(|s| str.push_str(s.as_str()));
                                let new_str = self.allocate_string(&str);
                                self.pop_value();
                                self.pop_value();
                                let v = Value::Object(new_str);
                                self.push_value(v);
                            } else {
                                self.runtime_error("Operands must be two numbers or two strings");
                                return Err(InterpretError::Runtime);
                            }
                        }
                        _ => {
                            self.runtime_error("Operands must be two numbers or two strings");
                            return Err(InterpretError::Runtime);
                        }
                    }
                }
                OpCode::Subtract => {
                    impl_vm_arithmetic_op!(self, -, Number);
                }
                OpCode::Multiply => {
                    impl_vm_arithmetic_op!(self, *, Number);
                }
                OpCode::Divide => {
                    impl_vm_arithmetic_op!(self, /, Number);
                }
                OpCode::Greater => {
                    impl_vm_arithmetic_op!(self, >, Bool);
                }
                OpCode::Less => {
                    impl_vm_arithmetic_op!(self, <, Bool);
                }
                OpCode::Nil => {
                    self.push_value(Value::Nil);
                }
                OpCode::True => {
                    self.push_value(Value::Bool(true));
                }
                OpCode::False => {
                    self.push_value(Value::Bool(false));
                }
                OpCode::Not => match self.pop_value() {
                    Some(v) => self.push_value(Value::Bool(v.is_falsey())),
                    _ => {
                        self.runtime_error("No operand available in the stack");
                        return Err(InterpretError::Runtime);
                    }
                },
                OpCode::Equal => {
                    let b = self.pop_value();
                    let a = self.pop_value();
                    match (a, b) {
                        (Some(va), Some(vb)) => {
                            self.push_value(Value::Bool(va == vb));
                        }
                        _ => {
                            self.runtime_error("Expression requires two operands from the stack");
                            return Err(InterpretError::Runtime);
                        }
                    }
                }
                OpCode::Print => {
                    if let Some(v) = self.pop_value() {
                        print_value(&v);
                        println!();
                    } else {
                        self.runtime_error("No value available on the stack for print statement");
                        return Err(InterpretError::Runtime);
                    }
                }
                OpCode::Pop => {
                    let _ = self.pop_value();
                }
                OpCode::DefineGlobal => {
                    if let Value::Object(object) = self.read_constant() {
                        if let ObjValue::String(str) = &object.object {
                            if let Some(v) = self.peek_value(0) {
                                self.stack.pop();
                                let key = InternedStrKey {
                                    ptr: NonNull::from(str.as_str()),
                                    gcobj: Some(object.clone()),
                                };
                                self.globals.insert(key, v);
                            } else {
                                self.runtime_error(
                                    "No value available on the stack for global decl",
                                );
                                return Err(InterpretError::Runtime);
                            }
                        } else {
                            self.runtime_error("Define global expects a constant string value");
                            return Err(InterpretError::Runtime);
                        }
                    } else {
                        self.runtime_error("Define global expects a constant string value");
                        return Err(InterpretError::Runtime);
                    }
                }
                OpCode::GetGlobal => {
                    if let Value::Object(obj) = self.read_constant() {
                        if let ObjValue::String(str) = &obj.object {
                            let key = InternedStrKey {
                                ptr: NonNull::from(str.as_str()),
                                gcobj: Some(obj.clone()),
                            };
                            let global = if let Some(v) = self.globals.get(&key) {
                                v.clone()
                            } else {
                                let str = format!("Undefined variable '{}'", str.as_str(),);
                                self.runtime_error(&str);
                                return Err(InterpretError::Runtime);
                            };
                            self.push_value(global);
                        } else {
                            self.runtime_error("Get global expects a constant string value");
                            return Err(InterpretError::Runtime);
                        }
                    } else {
                        self.runtime_error("Get global expects a constant string value");
                        return Err(InterpretError::Runtime);
                    }
                }
                OpCode::SetGlobal => {
                    if let Value::Object(obj) = self.read_constant() {
                        if let ObjValue::String(str) = &obj.object {
                            if let Some(v) = self.peek_value(0) {
                                let key = InternedStrKey {
                                    ptr: NonNull::from(str.as_str()),
                                    gcobj: Some(obj.clone()),
                                };
                                let prev = self.globals.insert(key.clone(), v);
                                if prev.is_none() {
                                    self.globals.remove(&key);
                                    let str = format!("Undefined variable '{}'", str.as_str(),);
                                    self.runtime_error(&str);
                                    return Err(InterpretError::Runtime);
                                }
                            } else {
                                self.runtime_error(
                                    "No value available on the stack for global decl",
                                );
                                return Err(InterpretError::Runtime);
                            }
                        } else {
                            self.runtime_error("Set global expects a constant string value");
                            return Err(InterpretError::Runtime);
                        }
                    } else {
                        self.runtime_error("Set global expects a constant string value");
                        return Err(InterpretError::Runtime);
                    }
                }
                OpCode::GetLocal => {
                    let slot = self.read_byte();
                    self.push_value(
                        self.stack[self.current_call_frame().slots + slot as usize].clone(),
                    );
                }
                OpCode::SetLocal => {
                    let slot = self.read_byte();
                    if let Some(v) = self.peek_value(0) {
                        let index = self.current_call_frame().slots + slot as usize;
                        self.stack[index] = v;
                    } else {
                        self.runtime_error("Could not peek stack top");
                        return Err(InterpretError::Runtime);
                    }
                }
                OpCode::JumpIfFalse => {
                    let offset = self.read_short();
                    if let Some(v) = self.peek_value(0) {
                        //if v.is_falsey() {
                        //    self.current_call_frame_mut().ip += offset as usize;
                        //}
                        // Same as the code above, but more efficient since it doesn't use if branching
                        self.current_call_frame_mut().ip += v.is_falsey_num() * offset as usize;
                    } else {
                        self.runtime_error("No value on the top of the stack");
                        return Err(InterpretError::Runtime);
                    }
                }
                OpCode::Jump => {
                    let offset = self.read_short();
                    self.current_call_frame_mut().ip += offset as usize;
                }
                OpCode::Loop => {
                    let offset = self.read_short();
                    self.current_call_frame_mut().ip -= offset as usize;
                }
                OpCode::Call => {
                    let arg_count = self.read_byte();
                    if let Some(v) = self.peek_value(arg_count as usize) {
                        if !self.call_value(v, arg_count) {
                            return Err(InterpretError::Runtime);
                        }
                    } else {
                        self.runtime_error("No value on the top of the stack");
                        return Err(InterpretError::Runtime);
                    }
                }
                OpCode::Closure => {
                    if let Value::Object(func) = self.read_constant() {
                        let mut closure = self.allocate_closure(func);
                        self.push_value(Value::Object(closure));
                        let upvalues_len = closure.with_closure(|c| c.upvalues.len());
                        for i in 0..upvalues_len {
                            let is_local = self.read_byte();
                            let index = self.read_byte();
                            if is_local == 1 {
                                closure.with_closure_mut(|cl| {
                                    cl.upvalues[i] = self.capture_upvalue(
                                        self.current_call_frame().slots + index as usize,
                                    );
                                });
                            } else {
                                let upvalue = self.current_call_frame().closure.upvalues[i].clone();
                                closure.with_closure_mut(move |cl| {
                                    cl.upvalues[i] = upvalue;
                                });
                            };
                        }
                    } else {
                        self.runtime_error("Expected function contstant");
                        return Err(InterpretError::Runtime);
                    }
                }
                OpCode::GetUpValue => {
                    let slot = self.read_byte();
                    let value =
                        match self.current_call_frame().closure.upvalues[slot as usize].deref() {
                            ObjUpValue::Ptr(index) => self.stack[*index].clone(),
                            ObjUpValue::Closed(v) => v.clone(),
                        };
                    self.push_value(value);
                }
                OpCode::SetUpValue => {
                    let slot = self.read_byte();
                    if let Some(v) = self.peek_value(0) {
                        let closure = &mut self.frames.last_mut().unwrap().closure;
                        match &mut closure.upvalues[slot as usize].deref_mut() {
                            ObjUpValue::Ptr(index) => self.stack[*index] = v,
                            ObjUpValue::Closed(val) => *val = v,
                        };
                    } else {
                        self.runtime_error("Expected value on top of the stack");
                        return Err(InterpretError::Runtime);
                    }
                }
                OpCode::CloseUpValue => {
                    self.close_upvalues(self.stack.len() - 1);
                    self.pop_value();
                }
            }
        }
    }

    fn mark_value(value: &mut Value, gray_list: &mut Vec<GCObjectPtr>) {
        if let Value::Object(obj) = value {
            Self::mark_gc_object(*obj, gray_list)
        }
    }

    fn mark_gc_object(mut gc_object: GCObjectPtr, gray_list: &mut Vec<GCObjectPtr>) {
        if gc_object.header.is_marked {
            return;
        }
        gc_object.mark();
        gray_list.push(gc_object);
    }

    fn mark_roots(&mut self) {
        // mark all values in the stack
        for v in self.stack.iter_mut() {
            Self::mark_value(v, &mut self.gray_stack);
        }

        // mark all globals
        // Work around for multiple write references to self, move hashmap to tmp value
        // and then back.
        for (key, value) in self.globals.iter_mut() {
            if let Some(key_obj) = key.gcobj {
                Self::mark_gc_object(key_obj, &mut self.gray_stack);
            }
            Self::mark_value(value, &mut self.gray_stack);
        }

        // mark all closures
        for frame in self.frames.iter_mut() {
            Self::mark_gc_object(frame.closure.ptr, &mut self.gray_stack);
        }

        // mark all open upvalues
        for upvalue in self.open_upvalues.iter_mut() {
            Self::mark_gc_object(upvalue.ptr, &mut self.gray_stack);
        }
    }

    fn blacken_object(&mut self, mut obj: GCObjectPtr) {
        #[cfg(feature = "debug_log_gc")]
        println!("{:?} blacken {:?}", obj.ptr.as_ptr(), obj.object);
        match &mut obj.object {
            ObjValue::NativeFunction(_) | ObjValue::String(_) => {}
            ObjValue::UpValue(upvalue) => {
                if let ObjUpValue::Closed(v) = upvalue {
                    Self::mark_value(v, &mut self.gray_stack);
                }
            }
            ObjValue::Function(func) => {
                if let Some(name) = func.name {
                    Self::mark_gc_object(name, &mut self.gray_stack);
                }
                for c in func.chunk.get_constants_mut() {
                    Self::mark_value(c, &mut self.gray_stack);
                }
            }
            ObjValue::Closure(cl) => {
                Self::mark_gc_object(cl.function.ptr, &mut self.gray_stack);
                for upvalue in cl.upvalues.iter_mut() {
                    Self::mark_gc_object(upvalue.ptr, &mut self.gray_stack);
                }
            }
        }
    }

    fn trace_references(&mut self) {
        while let Some(ptr) = self.gray_stack.pop() {
            self.blacken_object(ptr);
        }
    }

    fn sweep(&mut self) {
        let mut prev: Option<NonNull<GCObject>> = None;
        let mut current = self.objects.clone();
        while let Some(mut object) = current.clone() {
            if unsafe { object.as_ref().header.is_marked } {
                unsafe { object.as_mut().header.is_marked = false };
                prev = current;
                current = unsafe { object.as_mut().header.next };
            } else {
                let unreached = object;
                unsafe {
                    current = object.as_mut().header.next;
                    if let Some(mut prev_object) = prev {
                        prev_object.as_mut().header.next = current;
                    } else {
                        self.objects = current;
                    }
                }
                Self::deallocate(unreached, &mut self.bytes_allocated);
            }
        }
    }

    fn collect_garbage(&mut self) {
        const GC_HEAP_GROW_FACTOR: usize = 2;
        if self.is_compiling {
            return;
        }
        #[cfg(feature = "debug_log_gc")]
        println!("-- gc begin");
        let before = self.bytes_allocated;
        self.mark_roots();
        self.trace_references();
        self.remove_white_strings();
        self.sweep();

        self.next_gc = self.bytes_allocated * GC_HEAP_GROW_FACTOR;

        #[cfg(feature = "debug_log_gc")]
        {
            println!("-- gc end");
            let diff = if before < self.bytes_allocated {
                0
            } else {
                before - self.bytes_allocated
            };
            println!(
                "   collected {} bytes (from {} to {}) next at {}",
                diff, before, self.bytes_allocated, self.next_gc
            );
        }
    }

    fn free_objects(&mut self) {
        let mut cur_obj = self.objects.clone();
        while cur_obj.is_some() {
            let object = cur_obj.unwrap();
            cur_obj = unsafe { object.as_ref().header.next.clone() };
            Self::deallocate(object, &mut self.bytes_allocated);
        }
        self.string_table.clear();
    }

    fn deallocate(obj: NonNull<GCObject>, bytes_allocated: &mut usize) {
        unsafe {
            let to_drop = std::ptr::read(obj.as_ptr());
            #[cfg(feature = "debug_log_gc")]
            {
                println!("{:?} free {:?}", obj.as_ptr(), obj.as_ref().object);
            }
            std::mem::drop(to_drop);
            let layout = std::alloc::Layout::new::<GCObject>();
            *bytes_allocated -= layout.size();
            std::alloc::dealloc(obj.as_ptr() as *mut u8, layout);
        }
    }

    fn allocate(&mut self, obj: ObjValue) -> GCObjectPtr {
        #[cfg(feature = "stress_gc")]
        self.collect_garbage();

        unsafe {
            let layout = std::alloc::Layout::new::<GCObject>();

            self.bytes_allocated += layout.size();
            if self.bytes_allocated > self.next_gc {
                self.collect_garbage();
            }

            let ptr = std::alloc::alloc(layout) as *mut GCObject;
            if ptr.is_null() {
                panic!("Failed to allocate memory");
            }
            #[cfg(feature = "debug_log_gc")]
            println!(
                "{:?} alloc {} {:?}",
                ptr,
                std::alloc::Layout::new::<GCObject>().size(),
                obj
            );
            let gc_object = GCObject::new(obj);
            ptr.write(gc_object);

            if let Some(object) = self.objects {
                (*ptr).header.next = Some(object);
            }
            self.objects = Some(NonNull::new(ptr).unwrap());
            GCObjectPtr::new(NonNull::new(ptr).unwrap())
        }
    }

    fn allocate_closure(&mut self, function: GCObjectPtr) -> GCObjectPtr {
        if let ObjValue::Function(func) = &function.deref().object {
            let mut closure = Closure::new(function);
            closure.upvalues.resize(
                func.upvalue_count as usize,
                self.allocate_upvalue(ObjUpValue::new(usize::MAX))
                    .to_upvalue_view()
                    .unwrap(),
            );
            self.allocate(ObjValue::Closure(closure))
        } else {
            panic!("Function is not a function object");
        }
    }

    fn allocate_function(&mut self, name: Option<GCObjectPtr>) -> GCObjectPtr {
        let mut function = Function::new();
        function.name = name;
        self.allocate(ObjValue::Function(Function::new()))
    }

    fn allocate_string(&mut self, str: &str) -> GCObjectPtr {
        let string = Box::new(String::from(str));
        let mut key = InternedStrKey {
            ptr: NonNull::from(string.as_str()),
            gcobj: None,
        };

        // TODO: make this work with the entry api
        if let Some(v) = self.string_table.get(&key) {
            v.gcobj
        } else {
            let istr = InternedStr {
                ptr: NonNull::from(string.as_str()),
            };
            let gcobj = self.allocate(ObjValue::String(istr));
            key.gcobj = Some(gcobj.clone());
            self.string_table
                .insert(key, InternedStrData { gcobj, string });
            gcobj
        }
    }

    fn allocate_native_function(&mut self, native_fn: NativeFn) -> GCObjectPtr {
        self.allocate(ObjValue::NativeFunction(NativeFunction { func: native_fn }))
    }

    fn allocate_upvalue(&mut self, upvalue: ObjUpValue) -> GCObjectPtr {
        self.allocate(ObjValue::UpValue(upvalue))
    }

    fn remove_white_strings(&mut self) {
        let mut bytes_allocated = self.bytes_allocated;
        self.string_table.retain(|_, value| {
            if !value.gcobj.header.is_marked {
                VM::deallocate(value.gcobj.ptr, &mut bytes_allocated);
                return false;
            }
            true
        });
        self.bytes_allocated = bytes_allocated;
    }
}

impl Drop for VM {
    fn drop(&mut self) {
        self.shutdown();
    }
}

impl ObjectAllocator for VM {
    fn allocate_string(&mut self, str: &str) -> GCObjectPtr {
        self.allocate_string(str)
    }

    fn allocate_function(&mut self, name: Option<GCObjectPtr>) -> GCObjectPtr {
        self.allocate_function(name)
    }

    fn push_value(&mut self, v: Value) {
        self.push_value(v);
    }

    fn pop_value(&mut self) {
        self.pop_value();
    }
}
