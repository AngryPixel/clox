use crate::common::{Chunk, OpCode};
use crate::disassemble::disassemble_chunk;
use crate::scanner::{Scanner, Token, TokenType};
use crate::value::{
    ConstantIndex, Function, GCObjectPtr, ObjValue, ObjectAllocator, Value,
    CONSTANT_LONG_MAX_INDEX_VALUE,
};
use num_traits::FromPrimitive;
use std::cell::RefCell;
use std::ops::{Deref, DerefMut};
use std::ptr::NonNull;
use std::rc::Rc;
use std::str::FromStr;

#[derive(Debug, Eq, PartialEq, Copy, Clone, Ord, PartialOrd, FromPrimitive)]
enum Precedence {
    None,
    Assignment, // =
    Or,         // or
    And,        // and
    Equality,   // == !=
    Comparison, // < > <= >=
    Term,       // + -
    Factor,     // * /
    Unary,      // ! -
    Call,       // . ()
    Primary,
}

type ParseFn = fn(&mut Compiler);
type ParseFnPrefix = fn(&mut Compiler, bool);
struct ParseRule {
    prefix: Option<ParseFnPrefix>,
    infix: Option<ParseFn>,
    precedence: Precedence,
}

impl Default for ParseRule {
    fn default() -> Self {
        Self {
            prefix: None,
            infix: None,
            precedence: Precedence::None,
        }
    }
}

#[derive(Debug, Default)]
pub(crate) struct Parser<'a> {
    current: Token<'a>,
    previous: Token<'a>,
    had_error: bool,
    panic_mode: bool,
}

impl<'a> Parser<'a> {
    pub(crate) fn new() -> Self {
        Self {
            current: Token::default(),
            previous: Token::default(),
            had_error: false,
            panic_mode: false,
        }
    }
}

const MAX_LOCAL_COUNT: usize = u8::MAX as usize + 1;

fn identifiers_equal<'a>(t1: &Token<'a>, t2: &Token<'a>) -> bool {
    t1.text == t2.text
}

#[derive(Debug, Copy, Clone)]
struct Local<'a> {
    name: Token<'a>,
    depth: i32,
    is_captured: bool,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub(crate) enum FunctionType {
    Function,
    Script,
}

#[derive(Debug, Copy, Clone)]
struct UpValue {
    index: u8,
    is_local: bool,
}

pub(crate) struct Compiler<'a, 'b, 'c> {
    enclosing: Option<NonNull<Compiler<'a, 'b, 'c>>>,
    scanner: &'b mut Scanner<'a>,
    parser: &'b mut Parser<'a>,
    objects: Rc<RefCell<&'c mut dyn ObjectAllocator>>,
    locals: [Local<'a>; MAX_LOCAL_COUNT],
    upvalues: [UpValue; u8::MAX as usize],
    local_count: usize,
    scope_depth: i32,
    function: GCObjectPtr,
    function_type: FunctionType,
}

impl<'a, 'b, 'c> Compiler<'a, 'b, 'c> {
    pub(crate) fn new(
        enclosing: Option<NonNull<Compiler<'a, 'b, 'c>>>,
        scanner: &'b mut Scanner<'a>,
        parser: &'b mut Parser<'a>,
        function_type: FunctionType,
        objects: Rc<RefCell<&'c mut dyn ObjectAllocator>>,
    ) -> Self {
        let mut function = objects.borrow_mut().allocate_function(None);
        function.with_function_mut(|func| {
            if function_type == FunctionType::Function {
                let name_ptr = objects
                    .borrow_mut()
                    .allocate_string(&String::from_utf8_lossy(parser.previous.text));
                func.name = Some(name_ptr);
            }
        });
        let mut c = Self {
            enclosing,
            scanner,
            parser,
            objects,
            locals: [Local {
                name: Token::default(),
                depth: 0,
                is_captured: false,
            }; MAX_LOCAL_COUNT],
            upvalues: [UpValue {
                index: u8::MAX,
                is_local: false,
            }; u8::MAX as usize],
            local_count: 0,
            scope_depth: 0,
            function_type,
            function,
        };
        // local 0 reserved for the VM's own internal use
        let local = &mut c.locals[0];
        local.depth = 0;
        c.local_count += 1;
        c
    }

    fn on_error_at_current(&mut self, message: &str) {
        self.on_error_at(self.parser.current, message);
    }

    fn get_enclosing(&mut self) -> Option<&mut Compiler<'a, 'b, 'c>> {
        if let Some(mut ptr) = self.enclosing {
            unsafe { Some(ptr.as_mut()) }
        } else {
            None
        }
    }

    fn current_chunk_mut(&mut self) -> &mut Chunk {
        &mut self.current_function_mut().chunk
    }

    fn current_chunk(&self) -> &Chunk {
        &self.current_function().chunk
    }

    fn current_function(&self) -> &Function {
        match &self.function.deref().object {
            ObjValue::Function(func) => func,
            _ => panic!("Expected function object type"),
        }
    }

    fn current_function_mut(&mut self) -> &mut Function {
        match &mut self.function.deref_mut().object {
            ObjValue::Function(func) => func,
            _ => panic!("Expected function object type"),
        }
    }

    fn on_error(&mut self, message: &str) {
        self.on_error_at(self.parser.previous, message);
    }

    fn on_error_at(&mut self, token: Token, message: &str) {
        self.parser.panic_mode = true;
        eprint!("[line {:04}] Error", token.line);

        if token.ttype == TokenType::Eof {
            eprint!(" at end");
        } else if token.ttype == TokenType::Error {
            // nothing
        } else {
            eprint!(" at {}", String::from_utf8_lossy(token.text));
        }
        eprintln!(": {}", message);
        self.parser.had_error = true;
    }

    fn advance(&mut self) {
        self.parser.previous = self.parser.current;
        loop {
            self.parser.current = self.scanner.scan_token();
            if self.parser.current.ttype != TokenType::Error {
                break;
            }
            let str = String::from_utf8_lossy(self.parser.current.text);
            self.on_error_at_current(&str);
        }
    }

    fn consume(&mut self, tt: TokenType, message: &str) {
        if self.parser.current.ttype == tt {
            self.advance();
            return;
        }
        self.on_error_at_current(message);
    }

    fn check(&self, tt: TokenType) -> bool {
        self.parser.current.ttype == tt
    }

    fn do_match(&mut self, tt: TokenType) -> bool {
        if !self.check(tt) {
            return false;
        }
        self.advance();
        true
    }

    fn emit_byte(&mut self, byte: u8) {
        let prev_line = self.parser.previous.line;
        self.current_chunk_mut().write_byte(byte, prev_line);
    }

    fn emit_opcodes(&mut self, opcodes: &[OpCode]) {
        for opcode in opcodes {
            self.emit_byte(*opcode as u8);
        }
    }

    fn emit_jump(&mut self, opcode: OpCode) -> usize {
        self.emit_opcode(opcode);
        self.emit_byte(0xFF);
        self.emit_byte(0xFF);
        self.current_chunk_mut().get_opcodes().len() - 2
    }

    fn emit_loop(&mut self, loop_start: usize) {
        self.emit_opcode(OpCode::Loop);
        let offset = self.current_chunk_mut().get_opcodes().len() - loop_start + 2;
        if offset > u16::MAX as usize {
            self.on_error("Loop body too large");
            return;
        }
        self.emit_byte((offset >> 8 & 0xff) as u8);
        self.emit_byte((offset & 0xff) as u8);
    }

    fn emit_opcode(&mut self, opcode: OpCode) {
        self.emit_byte(opcode as u8);
    }

    fn emit_return(&mut self) {
        self.emit_opcode(OpCode::Nil);
        self.emit_opcode(OpCode::Return);
    }

    fn expression(&mut self) {
        self.parse_precedence(Precedence::Assignment);
    }

    fn block(&mut self) {
        while !self.check(TokenType::RightBrace) && !self.check(TokenType::Eof) {
            self.declaration();
        }
        self.consume(TokenType::RightBrace, "Expect '}' after block");
    }

    fn function(&mut self, function_type: FunctionType) {
        let mut compiler = Compiler::new(
            Some(NonNull::new(self as *mut Compiler<'a, 'b, 'c>).unwrap()),
            self.scanner,
            self.parser,
            function_type,
            self.objects.clone(),
        );
        compiler.begin_scope();
        compiler.consume(TokenType::LeftParen, "Expect '(' after function name");
        if !compiler.check(TokenType::RightParen) {
            loop {
                let func = compiler.current_function_mut();
                func.arity += 1;
                if func.arity > 255 {
                    compiler.on_error_at_current("Can't have more than 255 parameters");
                }

                let constant = compiler.parse_variable("Expected parameter name");
                compiler.define_variable(constant);
                if !compiler.do_match(TokenType::Comma) {
                    break;
                }
            }
        }
        compiler.consume(TokenType::RightParen, "Expect ')' after parameters.");
        compiler.consume(TokenType::LeftBrace, "Expect '{' before function body");
        compiler.block();
        let func = compiler.end_compiler();
        // TODO: multiple write reference to scanner
        let upvalues = &compiler.upvalues;
        self.emit_closure(Value::Object(func.clone()));

        match &func.deref().object {
            ObjValue::Function(function) => {
                for i in 0..function.upvalue_count {
                    let upvalue = upvalues[i as usize];
                    self.emit_byte(if upvalue.is_local { 1_u8 } else { 0_u8 });
                    self.emit_byte(upvalue.index);
                }
            }
            _ => unreachable!(),
        }
    }

    fn function_declaration(&mut self) {
        let global = self.parse_variable("Expect function name");
        self.mark_initialized();
        self.function(FunctionType::Function);
        self.define_variable(global);
    }

    fn var_declaration(&mut self) {
        let global = self.parse_variable("Expect variable name");

        if self.do_match(TokenType::Equal) {
            self.expression();
        } else {
            // transform decls such as 'var a;' into 'var a = nil;'
            self.emit_opcode(OpCode::Nil)
        }
        self.consume(
            TokenType::SemiColon,
            "Expect ',' after variable declaration",
        );

        self.define_variable(global);
    }

    fn expression_statement(&mut self) {
        self.expression();
        self.consume(TokenType::SemiColon, "Expect ';' after expression.");
        self.emit_opcode(OpCode::Pop);
    }

    fn for_statement(&mut self) {
        self.begin_scope();

        self.consume(TokenType::LeftParen, "Expect '(' after 'for'.");

        if self.do_match(TokenType::SemiColon) {
            // no initializer
        } else if self.do_match(TokenType::Var) {
            self.var_declaration();
        } else {
            self.expression_statement();
        }

        let mut loop_start = self.current_chunk_mut().get_opcodes().len();

        let mut exit_jump: Option<usize> = None;
        if !self.do_match(TokenType::SemiColon) {
            self.expression();
            self.consume(TokenType::SemiColon, "Expect ';' after loop condition");

            // jump out of loop if condition is false;
            exit_jump = Some(self.emit_jump(OpCode::JumpIfFalse));
            self.emit_opcode(OpCode::Pop);
        }

        if !self.do_match(TokenType::RightParen) {
            // Since we can't generate the condition loop at the end, we just generate code to skip
            // the increment straight into the body. when the body is finished it will jump to the
            // increment block and then back to the loop start.
            let body_jump = self.emit_jump(OpCode::Jump);
            let increment_start = self.current_chunk_mut().get_opcodes().len();
            self.expression();
            self.emit_opcode(OpCode::Pop);
            self.consume(TokenType::RightParen, "Expect ')' after 'clauses.");

            self.emit_loop(loop_start);
            loop_start = increment_start;
            self.patch_jump(body_jump);
        }

        self.statement();

        self.emit_loop(loop_start);

        if let Some(ej) = exit_jump {
            self.patch_jump(ej);
            self.emit_opcode(OpCode::Pop);
        }

        self.end_scope();
    }

    fn if_statement(&mut self) {
        self.consume(TokenType::LeftParen, "Expect '(' after 'if'.");
        self.expression();
        self.consume(TokenType::RightParen, "Expect ')' after condition");

        let then_jump = self.emit_jump(OpCode::JumpIfFalse);
        self.emit_opcode(OpCode::Pop);
        self.statement();
        let else_jump = self.emit_jump(OpCode::Jump);
        self.patch_jump(then_jump);
        self.emit_opcode(OpCode::Pop);
        if self.do_match(TokenType::Else) {
            self.statement();
        }
        self.patch_jump(else_jump);
    }

    fn print_statement(&mut self) {
        self.expression();
        self.consume(TokenType::SemiColon, "Expect ';' after value.");
        self.emit_opcode(OpCode::Print);
    }

    fn return_statement(&mut self) {
        if self.function_type == FunctionType::Script {
            self.on_error("Can't return from top-level code");
        }
        if self.do_match(TokenType::SemiColon) {
            self.emit_return();
        } else {
            self.expression();
            self.consume(TokenType::SemiColon, "Expect ';' after return value");
            self.emit_opcode(OpCode::Return);
        }
    }

    fn while_statement(&mut self) {
        let loop_start = self.current_chunk_mut().get_opcodes().len();

        self.consume(TokenType::LeftParen, "Expect '(' after 'while'.");
        self.expression();
        self.consume(TokenType::RightParen, "Expect ')' after condition");

        let exit_jump = self.emit_jump(OpCode::JumpIfFalse);

        self.emit_opcode(OpCode::Pop);
        self.statement();

        self.emit_loop(loop_start);

        self.patch_jump(exit_jump);
        self.emit_opcode(OpCode::Pop);
    }

    // Skip current error on decl/statement/expression until we reach a start of new decl/statement
    fn synchronize(&mut self) {
        self.parser.panic_mode = false;

        while self.parser.current.ttype != TokenType::Eof {
            if self.parser.previous.ttype == TokenType::SemiColon {
                return;
            }
            match self.parser.current.ttype {
                TokenType::Class
                | TokenType::Function
                | TokenType::Var
                | TokenType::For
                | TokenType::If
                | TokenType::While
                | TokenType::Print
                | TokenType::Return => return,
                _ => {}
            };
            self.advance();
        }
    }

    fn declaration(&mut self) {
        if self.do_match(TokenType::Function) {
            self.function_declaration();
        } else if self.do_match(TokenType::Var) {
            self.var_declaration();
        } else {
            self.statement();
        }

        if self.parser.panic_mode {
            self.synchronize();
        }
    }

    fn statement(&mut self) {
        if self.do_match(TokenType::Print) {
            self.print_statement();
        } else if self.do_match(TokenType::For) {
            self.for_statement();
        } else if self.do_match(TokenType::If) {
            self.if_statement();
        } else if self.do_match(TokenType::Return) {
            self.return_statement();
        } else if self.do_match(TokenType::While) {
            self.while_statement();
        } else if self.do_match(TokenType::LeftBrace) {
            self.begin_scope();
            self.block();
            self.end_scope();
        } else {
            self.expression_statement();
        }
    }

    fn make_constant(&mut self, v: Value) -> ConstantIndex {
        let index = self.current_chunk_mut().add_constant(v);
        if index > CONSTANT_LONG_MAX_INDEX_VALUE {
            self.on_error("Too many constants in one chunk");
            return 0;
        }
        index
    }

    fn emit_constant(&mut self, v: Value) {
        self.objects.borrow_mut().push_value(v.clone());
        let constant_index = self.make_constant(v);
        let prev_line = self.parser.previous.line;
        self.current_chunk_mut()
            .write_constant(constant_index, prev_line);
        self.objects.borrow_mut().pop_value();
    }

    fn emit_closure(&mut self, v: Value) {
        let constant_index = self.make_constant(v);
        debug_assert!(constant_index < 256);
        let prev_line = self.parser.previous.line;
        let chunk = self.current_chunk_mut();
        chunk.write(OpCode::Closure, prev_line);
        chunk.write_byte(constant_index as u8, prev_line);
    }

    fn patch_jump(&mut self, offset: usize) {
        // -2 to adjust for the bytecode for the jump itself
        let jump = self.current_chunk_mut().get_opcodes().len() - offset - 2;

        if jump > u16::MAX as usize {
            self.on_error("Too much code to jump over.");
            return;
        }

        self.current_chunk_mut().patch_jump(offset, jump as u16);
    }

    fn end_compiler(&mut self) -> GCObjectPtr {
        self.emit_return();
        #[cfg(feature = "debug_print_code")]
        if !self.parser.had_error {
            let fn_name = self.function.deref().with_function(|f| {
                if let Some(str) = f.name {
                    str.with_string(|s| String::from(s.as_str()))
                } else {
                    String::from("<script>")
                }
            });
            disassemble_chunk(&self.current_chunk(), &fn_name);
        }
        self.function
    }

    fn begin_scope(&mut self) {
        self.scope_depth += 1;
    }

    fn end_scope(&mut self) {
        debug_assert!(self.scope_depth > 0);
        self.scope_depth -= 1;

        while self.local_count > 0 && self.locals[self.local_count - 1].depth > self.scope_depth {
            if self.locals[self.local_count - 1].is_captured {
                self.emit_opcode(OpCode::CloseUpValue);
            } else {
                self.emit_opcode(OpCode::Pop);
            }
            self.local_count -= 1;
        }
    }

    fn grouping(&mut self) {
        // assumes ( has been consumed
        self.expression();
        self.consume(TokenType::RightParen, "Expect ')' after expression.");
    }

    fn number(&mut self) {
        let str = String::from_utf8_lossy(self.parser.previous.text);
        let value = f64::from_str(&str).expect("Failed to convert text to f64");
        self.emit_constant(Value::Number(value));
    }

    fn string(&mut self) {
        let slice = &self.parser.previous.text[1..self.parser.previous.text.len() - 1];
        let str = String::from_utf8_lossy(slice);
        let gcstr = self.objects.borrow_mut().allocate_string(&str);
        self.emit_constant(Value::Object(gcstr));
    }

    fn named_variable(&mut self, token: &Token<'a>, can_assign: bool) {
        let get_op: OpCode;
        let set_op: OpCode;
        let arg = if let Some(local) = self.resolve_local(token) {
            set_op = OpCode::SetLocal;
            get_op = OpCode::GetLocal;
            local
        } else if let Some(upvalue) = self.resolve_upvalue(token) {
            set_op = OpCode::SetUpValue;
            get_op = OpCode::GetUpValue;
            upvalue
        } else {
            set_op = OpCode::SetGlobal;
            get_op = OpCode::GetGlobal;
            self.identifier_constant(token)
        };

        debug_assert!(arg < 255);
        if can_assign && self.do_match(TokenType::Equal) {
            self.expression();
            self.emit_opcode(set_op);
        } else {
            self.emit_opcode(get_op);
        }
        self.emit_byte(arg as u8);
    }

    fn resolve_local(&mut self, token: &Token<'a>) -> Option<ConstantIndex> {
        if self.local_count == 0 {
            return None;
        }
        for i in 0..self.local_count {
            let index = self.local_count - 1 - i;
            let local = &self.locals[index];
            if identifiers_equal(token, &local.name) {
                if local.depth == -1 {
                    self.on_error("Can't read local variable in its own initializer");
                }
                return Some(index as ConstantIndex);
            }
        }
        None
    }

    fn add_upvalue(&mut self, index: u32, is_local: bool) -> ConstantIndex {
        debug_assert!(index < u8::MAX as u32);
        let upvalue_count = self.current_function_mut().upvalue_count;

        for i in 0..upvalue_count {
            let upvalue = &mut self.upvalues[i as usize];
            if upvalue.index == index as u8 && upvalue.is_local == is_local {
                return i;
            }
        }

        if upvalue_count > u8::MAX as u32 {
            self.on_error("Too many closure variables in function");
            return 0;
        }

        self.upvalues[upvalue_count as usize].is_local = is_local;
        self.upvalues[upvalue_count as usize].index = index as u8;
        self.current_function_mut().upvalue_count += 1;
        upvalue_count
    }

    fn resolve_upvalue(&mut self, token: &Token<'a>) -> Option<ConstantIndex> {
        if let Some(enclosing) = self.get_enclosing() {
            if let Some(index) = enclosing.resolve_local(token) {
                enclosing.locals[index as usize].is_captured = true;
                Some(self.add_upvalue(index, true))
            } else {
                if let Some(index) = enclosing.resolve_upvalue(token) {
                    Some(self.add_upvalue(index, false))
                } else {
                    None
                }
            }
        } else {
            None
        }
    }

    fn variable(&mut self, can_assign: bool) {
        let prev_token = self.parser.previous;
        self.named_variable(&prev_token, can_assign);
    }

    fn get_rule(&mut self, tt: TokenType) -> ParseRule {
        let group_fn = |c: &mut Compiler, _: bool| {
            c.grouping();
        };
        let unary_fn = |c: &mut Compiler, _: bool| {
            c.unary();
        };
        let binary_fn = |c: &mut Compiler| {
            c.binary();
        };
        let number_fn = |c: &mut Compiler, _: bool| {
            c.number();
        };
        let literal_fn = |c: &mut Compiler, _: bool| {
            c.literal();
        };
        let string_fn = |c: &mut Compiler, _: bool| {
            c.string();
        };
        let variable_fn = |c: &mut Compiler, can_assign: bool| {
            c.variable(can_assign);
        };
        match tt {
            TokenType::LeftParen => ParseRule {
                prefix: Some(group_fn),
                infix: Some(|c: &mut Compiler| c.call()),
                precedence: Precedence::Call,
            },
            TokenType::Minus => ParseRule {
                prefix: Some(unary_fn),
                infix: Some(binary_fn),
                precedence: Precedence::Term,
            },
            TokenType::Plus => ParseRule {
                prefix: Some(unary_fn),
                infix: Some(binary_fn),
                precedence: Precedence::Term,
            },
            TokenType::Star => ParseRule {
                prefix: None,
                infix: Some(binary_fn),
                precedence: Precedence::Factor,
            },
            TokenType::Slash => ParseRule {
                prefix: None,
                infix: Some(binary_fn),
                precedence: Precedence::Factor,
            },
            TokenType::Number => ParseRule {
                prefix: Some(number_fn),
                infix: None,
                precedence: Precedence::None,
            },
            TokenType::False => ParseRule {
                prefix: Some(literal_fn),
                infix: None,
                precedence: Precedence::None,
            },
            TokenType::True => ParseRule {
                prefix: Some(literal_fn),
                infix: None,
                precedence: Precedence::None,
            },
            TokenType::Nil => ParseRule {
                prefix: Some(literal_fn),
                infix: None,
                precedence: Precedence::None,
            },
            TokenType::Bang => ParseRule {
                prefix: Some(unary_fn),
                infix: None,
                precedence: Precedence::None,
            },
            TokenType::BangEqual => ParseRule {
                prefix: None,
                infix: Some(binary_fn),
                precedence: Precedence::Equality,
            },
            TokenType::EqualEqual => ParseRule {
                prefix: None,
                infix: Some(binary_fn),
                precedence: Precedence::Equality,
            },
            TokenType::Greater => ParseRule {
                prefix: None,
                infix: Some(binary_fn),
                precedence: Precedence::Comparison,
            },
            TokenType::GreaterEqual => ParseRule {
                prefix: None,
                infix: Some(binary_fn),
                precedence: Precedence::Comparison,
            },
            TokenType::Less => ParseRule {
                prefix: None,
                infix: Some(binary_fn),
                precedence: Precedence::Comparison,
            },
            TokenType::LessEqual => ParseRule {
                prefix: None,
                infix: Some(binary_fn),
                precedence: Precedence::Comparison,
            },
            TokenType::String => ParseRule {
                prefix: Some(string_fn),
                infix: None,
                precedence: Precedence::Comparison,
            },
            TokenType::Identifier => ParseRule {
                prefix: Some(variable_fn),
                infix: None,
                precedence: Precedence::None,
            },
            TokenType::And => ParseRule {
                prefix: None,
                infix: Some(|c: &mut Compiler| {
                    c.and_();
                }),
                precedence: Precedence::And,
            },
            TokenType::Or => ParseRule {
                prefix: None,
                infix: Some(|c: &mut Compiler| {
                    c.or_();
                }),
                precedence: Precedence::Or,
            },
            _ => ParseRule::default(),
        }
    }

    fn unary(&mut self) {
        let operator_type = self.parser.previous.ttype;
        // Compile operand
        self.parse_precedence(Precedence::Unary);
        // Emit the operator instruction
        match operator_type {
            TokenType::Minus => self.emit_opcode(OpCode::Negate),
            TokenType::Bang => self.emit_opcode(OpCode::Not),
            _ => {}
        };
    }

    fn binary(&mut self) {
        let operator_type = self.parser.previous.ttype;

        // compile the right operand
        let rule = self.get_rule(operator_type);
        self.parse_precedence(FromPrimitive::from_u8((rule.precedence as u8) + 1).unwrap());

        match operator_type {
            TokenType::Plus => self.emit_opcode(OpCode::Add),
            TokenType::Minus => self.emit_opcode(OpCode::Subtract),
            TokenType::Slash => self.emit_opcode(OpCode::Divide),
            TokenType::Star => self.emit_opcode(OpCode::Multiply),
            TokenType::BangEqual => self.emit_opcodes(&[OpCode::Equal, OpCode::Not]),
            TokenType::EqualEqual => self.emit_opcode(OpCode::Equal),
            TokenType::Greater => self.emit_opcode(OpCode::Greater),
            TokenType::GreaterEqual => self.emit_opcodes(&[OpCode::Less, OpCode::Not]),
            TokenType::Less => self.emit_opcode(OpCode::Less),
            TokenType::LessEqual => self.emit_opcodes(&[OpCode::Greater, OpCode::Not]),
            _ => {}
        };
    }

    fn call(&mut self) {
        let arg_count = self.argument_list();
        self.emit_opcode(OpCode::Call);
        self.emit_byte(arg_count);
    }

    fn literal(&mut self) {
        match self.parser.previous.ttype {
            TokenType::False => self.emit_opcode(OpCode::False),
            TokenType::True => self.emit_opcode(OpCode::True),
            TokenType::Nil => self.emit_opcode(OpCode::Nil),
            _ => {}
        }
    }

    fn parse_precedence(&mut self, precedence: Precedence) {
        // From Chap 17.6.1 - Pratt Parsing
        // At the beginning of parsePrecedence(), we look up a prefix parser for the current token.
        // The first token is always going to belong to some kind of prefix expression, by definition.
        // It may turn out to be nested as an operand inside one or more infix expressions,
        // but as you read the code from left to right, the first token you hit always belongs to a
        // prefix expression.
        // After parsing that, which may consume more tokens, the prefix expression is done. Now we
        // look for an infix parser for the next token. If we find one, it means the prefix expression
        // we already compiled might be an operand for it. But only if the call to parsePrecedence()
        // has a precedence that is low enough to permit that infix operator
        // If the next token is too low precedence, or isn’t an infix operator at all, we’re done.
        // We’ve parsed as much expression as we can. Otherwise, we consume the operator and hand
        // off control to the infix parser we found. It consumes whatever other tokens it needs
        // (usually the right operand) and returns back to parsePrecedence(). Then we loop back
        // around and see if the next token is also a valid infix operator that can take the entire
        // preceding expression as its operand. We keep looping like that, crunching through infix
        // operators and their operands until we hit a token that isn’t an infix operator or is too
        // low precedence and stop
        self.advance();
        let rule = self.get_rule(self.parser.previous.ttype);
        let can_assign = precedence <= Precedence::Assignment;
        if let Some(prefix) = rule.prefix {
            // Ensure assignment operator is handled properly in expressions such as
            // a * b = c + d;
            (prefix)(self, can_assign);
        } else {
            self.on_error("Expect expression");
            return;
        }
        while precedence <= self.get_rule(self.parser.current.ttype).precedence {
            self.advance();
            let infix_rule = self.get_rule(self.parser.previous.ttype);
            (infix_rule.infix.expect("Expected valid infix rule"))(self);
        }

        if can_assign && self.do_match(TokenType::Equal) {
            self.on_error("Invalid assignment target.");
        }
    }

    fn parse_variable(&mut self, error_message: &str) -> ConstantIndex {
        self.consume(TokenType::Identifier, error_message);

        self.declare_variable();
        if self.scope_depth > 0 {
            return 0;
        }

        let prev_token = self.parser.previous;
        return self.identifier_constant(&prev_token);
    }

    fn mark_initialized(&mut self) {
        // Scope 0 is reserved for the vm
        if self.scope_depth == 0 {
            return;
        }
        self.locals[self.local_count - 1].depth = self.scope_depth;
    }

    fn define_variable(&mut self, global: ConstantIndex) {
        if self.scope_depth > 0 {
            // Not a global variable
            self.mark_initialized();
            return;
        }
        debug_assert!(global < 255);
        // TODO: every time we access a global we push a new string constant into the constant list,
        // this will quickly exhaust the counter. Optimize this by checking whether a global already
        // exists
        self.emit_opcode(OpCode::DefineGlobal);
        self.emit_byte(global as u8);
    }

    fn argument_list(&mut self) -> u8 {
        let mut arg_count: u8 = 0;
        if !self.check(TokenType::RightParen) {
            loop {
                self.expression();
                if arg_count == 255 {
                    self.on_error("Can't have more than 255 arguments");
                }
                arg_count += 1;
                if !self.do_match(TokenType::Comma) {
                    break;
                }
            }
        }
        self.consume(TokenType::RightParen, "Expect ')' after arguments");
        arg_count
    }

    fn and_(&mut self) {
        let end_jump = self.emit_jump(OpCode::JumpIfFalse);
        self.emit_opcode(OpCode::Pop);
        self.parse_precedence(Precedence::And);
        self.patch_jump(end_jump);
    }

    fn or_(&mut self) {
        let else_jump = self.emit_jump(OpCode::JumpIfFalse);
        let end_jump = self.emit_jump(OpCode::Jump);

        self.patch_jump(else_jump);
        self.emit_opcode(OpCode::Pop);

        self.parse_precedence(Precedence::Or);
        self.patch_jump(end_jump);
    }

    fn identifier_constant(&mut self, token: &Token<'a>) -> ConstantIndex {
        let str = String::from_utf8_lossy(token.text);
        let gc_str = self.objects.borrow_mut().allocate_string(&str);
        return self.make_constant(Value::Object(gc_str));
    }

    fn add_local(&mut self, token: &Token<'a>) {
        if self.local_count == MAX_LOCAL_COUNT {
            self.on_error("Too many local variables in function");
            return;
        }
        let local = &mut self.locals[self.local_count];
        self.local_count += 1;
        local.name = token.clone();
        // protect against 'var a = a;' when a is not initialized yet
        local.depth = -1;
    }

    fn declare_variable(&mut self) {
        if self.scope_depth == 0 {
            // Not a local variable
            return;
        }
        let prev_token = self.parser.previous;

        if self.local_count > 0 {
            for i in 0..self.local_count {
                let index = self.local_count - 1 - i;
                let local = &self.locals[index];
                if local.depth != -1 && local.depth < self.scope_depth {
                    break;
                }
                if identifiers_equal(&prev_token, &local.name) {
                    self.on_error("Already variable with this name in this scope");
                }
            }
        }

        self.add_local(&prev_token);
    }

    pub(crate) fn compile(&mut self) -> Result<GCObjectPtr, ()> {
        self.current_chunk_mut().clear();
        self.advance();

        while !self.do_match(TokenType::Eof) {
            self.declaration();
        }
        let function = self.end_compiler();
        if self.parser.had_error {
            Err(())
        } else {
            Ok(function)
        }
    }
}
