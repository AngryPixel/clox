use crate::common::Chunk;
use std::fmt::{Debug, Formatter};
use std::hash::{Hash, Hasher};
use std::ops::{Deref, DerefMut};
use std::ptr::NonNull;

pub type Constant = f64;

#[derive(Debug, Clone, PartialEq)]
pub struct ObjectHeader {
    pub(crate) next: Option<NonNull<GCObject>>,
    pub(crate) is_marked: bool,
}

#[derive(Debug)]
pub enum ObjValue {
    String(InternedStr),
    Function(Function),
    NativeFunction(NativeFunction),
    Closure(Closure),
    UpValue(ObjUpValue),
}

#[derive(Debug)]
pub struct GCObject {
    pub(crate) header: ObjectHeader,
    pub(crate) object: ObjValue,
}

impl GCObject {
    pub(crate) fn new(obj: ObjValue) -> Self {
        Self {
            header: ObjectHeader {
                next: None,
                is_marked: false,
            },
            object: obj,
        }
    }
    pub(crate) fn mark(&mut self) {
        self.header.is_marked = true;
        #[cfg(feature = "debug_log_gc")]
        println!("{:?} mark {:?}", self as *const GCObject, self.object);
    }

    pub(crate) fn with_function_mut<Z, T: FnOnce(&mut Function) -> Z>(&mut self, cb: T) -> Z {
        match &mut self.object {
            ObjValue::Function(v) => cb(v),
            _ => panic!("Value is not a function"),
        }
    }

    pub(crate) fn with_function<Z, T: FnOnce(&Function) -> Z>(&self, cb: T) -> Z {
        match &self.object {
            ObjValue::Function(v) => cb(v),
            _ => panic!("Value is not a function"),
        }
    }

    pub(crate) fn with_closure_mut<Z, T: FnOnce(&mut Closure) -> Z>(&mut self, cb: T) -> Z {
        match &mut self.object {
            ObjValue::Closure(v) => cb(v),
            _ => panic!("Value is not a closure"),
        }
    }

    pub(crate) fn with_closure<Z, T: FnOnce(&Closure) -> Z>(&self, cb: T) -> Z {
        match &self.object {
            ObjValue::Closure(v) => cb(v),
            _ => panic!("Value is not a closure"),
        }
    }

    pub(crate) fn with_string_mut<Z, T: FnOnce(&mut InternedStr) -> Z>(&mut self, cb: T) -> Z {
        match &mut self.object {
            ObjValue::String(v) => cb(v),
            _ => panic!("Value is not a string"),
        }
    }

    pub(crate) fn with_string<Z, T: FnOnce(&InternedStr) -> Z>(&self, cb: T) -> Z {
        match &self.object {
            ObjValue::String(v) => cb(v),
            _ => panic!("Value is not a string"),
        }
    }

    pub(crate) fn is_string(&self) -> bool {
        match &self.object {
            ObjValue::String(_) => true,
            _ => false,
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct GCObjectPtr {
    pub(crate) ptr: NonNull<GCObject>,
}

impl GCObjectPtr {
    pub(crate) fn new(ptr: NonNull<GCObject>) -> Self {
        Self { ptr }
    }

    pub(crate) fn to_function_view(&self) -> Option<GCObjectPtrView<Function>> {
        match &self.object {
            ObjValue::Function(f) => Some(GCObjectPtrView {
                ptr: self.clone(),
                view: NonNull::from(f),
            }),
            _ => None,
        }
    }

    pub(crate) fn to_closure_view(&self) -> Option<GCObjectPtrView<Closure>> {
        match &self.object {
            ObjValue::Closure(c) => Some(GCObjectPtrView {
                ptr: self.clone(),
                view: NonNull::from(c),
            }),
            _ => None,
        }
    }

    pub(crate) fn to_upvalue_view(&self) -> Option<GCObjectPtrView<ObjUpValue>> {
        match &self.object {
            ObjValue::UpValue(u) => Some(GCObjectPtrView {
                ptr: self.clone(),
                view: NonNull::from(u),
            }),
            _ => None,
        }
    }
}

impl Deref for GCObjectPtr {
    type Target = GCObject;
    fn deref(&self) -> &Self::Target {
        unsafe { self.ptr.as_ref() }
    }
}

impl DerefMut for GCObjectPtr {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { self.ptr.as_mut() }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub(crate) struct GCObjectPtrView<T> {
    pub(crate) ptr: GCObjectPtr,
    view: NonNull<T>,
}

impl Deref for GCObjectPtrView<Function> {
    type Target = Function;
    fn deref(&self) -> &Self::Target {
        unsafe { self.view.as_ref() }
    }
}
impl DerefMut for GCObjectPtrView<Function> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { self.view.as_mut() }
    }
}

impl Deref for GCObjectPtrView<Closure> {
    type Target = Closure;
    fn deref(&self) -> &Self::Target {
        unsafe { self.view.as_ref() }
    }
}
impl DerefMut for GCObjectPtrView<Closure> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { self.view.as_mut() }
    }
}

impl Deref for GCObjectPtrView<ObjUpValue> {
    type Target = ObjUpValue;
    fn deref(&self) -> &Self::Target {
        unsafe { self.view.as_ref() }
    }
}
impl DerefMut for GCObjectPtrView<ObjUpValue> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { self.view.as_mut() }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    Nil,
    Bool(bool),
    Number(Constant),
    Object(GCObjectPtr),
}

impl Value {
    pub fn is_falsey(&self) -> bool {
        match self {
            Value::Nil => true,
            Value::Bool(b) => !*b,
            _ => false,
        }
    }

    pub fn is_falsey_num(&self) -> usize {
        match self {
            Value::Nil => 1,
            Value::Bool(b) => !*b as usize,
            _ => 0,
        }
    }

    pub fn is_nil(&self) -> bool {
        match self {
            Value::Nil => true,
            _ => false,
        }
    }

    pub fn is_bool(&self) -> bool {
        match self {
            Value::Bool(_) => true,
            _ => false,
        }
    }

    pub fn is_number(&self) -> bool {
        match self {
            Value::Number(_) => true,
            _ => false,
        }
    }

    pub fn is_object(&self) -> bool {
        match self {
            Value::Object(_) => true,
            _ => false,
        }
    }

    pub fn is_function(&self) -> bool {
        match self {
            Value::Object(obj) => match &obj.deref().object {
                ObjValue::Function(_) => true,
                _ => false,
            },
            _ => false,
        }
    }

    pub fn is_closure(&self) -> bool {
        match self {
            Value::Object(obj) => match &obj.deref().object {
                ObjValue::Closure(_) => true,
                _ => false,
            },
            _ => false,
        }
    }

    pub fn is_native_function(&self) -> bool {
        match self {
            Value::Object(obj) => match &obj.deref().object {
                ObjValue::NativeFunction(_) => true,
                _ => false,
            },
            _ => false,
        }
    }

    pub fn with_function_mut<T: FnOnce(&mut Function)>(&mut self, cb: T) {
        match self {
            Value::Object(obj) => match &mut obj.deref_mut().object {
                ObjValue::Function(v) => cb(v),
                _ => panic!("Value is not a function"),
            },
            _ => panic!("Value is not a function"),
        }
    }

    pub fn with_function<T: FnOnce(&Function)>(&self, cb: T) {
        match self {
            Value::Object(obj) => match &obj.deref().object {
                ObjValue::Function(v) => cb(v),
                _ => panic!("Value is not a function"),
            },
            _ => panic!("Value is not a function"),
        }
    }

    pub fn with_closure_mut<T: FnOnce(&mut Closure)>(&mut self, cb: T) {
        match self {
            Value::Object(obj) => match &mut obj.deref_mut().object {
                ObjValue::Closure(v) => cb(v),
                _ => panic!("Value is not a function"),
            },
            _ => panic!("Value is not a function"),
        }
    }

    pub fn with_closure<T: FnOnce(&Closure)>(&self, cb: T) {
        match self {
            Value::Object(obj) => match &obj.deref().object {
                ObjValue::Closure(v) => cb(v),
                _ => panic!("Value is not a function"),
            },
            _ => panic!("Value is not a function"),
        }
    }
}

fn print_function(function: &Function) {
    if let Some(gcobject) = &function.name {
        if let ObjValue::String(str) = &gcobject.deref().object {
            print!("<fn  {}>", str.as_str());
        } else {
            panic!("Not a string object");
        }
    } else {
        print!("<script>");
    }
}

pub fn print_value(v: &Value) {
    match v {
        Value::Nil | Value::Bool(_) | Value::Number(_) => {
            print!("{}", v);
        }
        Value::Object(obj) => match &obj.deref().object {
            ObjValue::String(sym) => {
                print!("{}", sym.as_str());
            }
            ObjValue::Function(func) => {
                print_function(func.deref());
            }
            ObjValue::Closure(cl) => {
                print_function(cl.function.deref());
            }
            ObjValue::NativeFunction(_) => {
                print!("<native fn>");
            }
            ObjValue::UpValue(_) => {
                print!("upvalue");
            }
        },
    }
}

impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Value::Nil => write!(f, "Nil"),
            Value::Bool(b) => write!(f, "{}", b),
            Value::Number(n) => write!(f, "{}", n),
            Value::Object(obj) => match &obj.deref().object {
                ObjValue::String(sym) => {
                    write!(f, "Str:{:?}", sym)
                }
                ObjValue::Function(_) => {
                    write!(f, "<fn>")
                }
                ObjValue::Closure(_) => {
                    write!(f, "<closure>")
                }
                ObjValue::NativeFunction(_) => {
                    write!(f, "<native fn>")
                }
                ObjValue::UpValue(_) => {
                    write!(f, "upvalue")
                }
            },
        }
    }
}

impl From<Constant> for Value {
    fn from(v: f64) -> Self {
        Value::Number(v)
    }
}

impl From<bool> for Value {
    fn from(v: bool) -> Self {
        Value::Bool(v)
    }
}

pub type ValueArray = Vec<Value>;
pub type ConstantIndex = u32;

pub const CONSTANT_SHORT_MAX_INDEX_VALUE: u8 = u8::MAX;
pub const CONSTANT_LONG_MAX_INDEX_VALUE: u32 = (1 << 24) - 1;
pub const CONSTANT_LONG_MASK: u32 = (1 << 24) - 1;

#[derive(Debug)]
pub struct Function {
    pub arity: i32,
    pub chunk: Chunk,
    pub name: Option<GCObjectPtr>,
    pub upvalue_count: u32,
}

impl Function {
    pub fn new() -> Self {
        Self {
            arity: 0,
            chunk: Chunk::new(),
            name: None,
            upvalue_count: 0,
        }
    }
}

pub type NativeFn = fn(&mut [Value]) -> Value;

#[derive(Copy, Clone)]
pub struct NativeFunction {
    pub func: NativeFn,
}

impl PartialEq for NativeFunction {
    // Rust doesn't support comparing function pointers natively LLVM may perform some optimizations
    // where some functions with the same body get merged together.
    fn eq(&self, other: &Self) -> bool {
        return self.func as *const NativeFn == other.func as *const NativeFn;
    }
    fn ne(&self, other: &Self) -> bool {
        return self.func as *const NativeFn != other.func as *const NativeFn;
    }
}

impl Debug for NativeFunction {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Native FN {:?}", self.func as *const NativeFn)
    }
}

#[derive(Debug)]
pub struct Closure {
    pub(crate) function: GCObjectPtrView<Function>,
    pub(crate) upvalues: Vec<GCObjectPtrView<ObjUpValue>>,
}

impl Closure {
    pub fn new(func: GCObjectPtr) -> Self {
        Self {
            function: func.to_function_view().unwrap(),
            upvalues: Vec::new(),
        }
    }
}

// The tutorial has pointer to store a Value here, we use a relative offset instead
#[derive(Debug, Clone, PartialEq)]
pub enum ObjUpValue {
    Ptr(usize),
    Closed(Value),
}

impl ObjUpValue {
    pub fn new(index: usize) -> Self {
        Self::Ptr(index)
    }
}

#[derive(Debug, Eq, PartialEq, Hash, Copy, Clone)]
pub struct InternedStr {
    pub(crate) ptr: NonNull<str>,
}

impl InternedStr {
    pub fn as_str(&self) -> &str {
        unsafe { self.ptr.as_ref() }
    }
}

#[derive(Debug)]
pub(crate) struct InternedStrData {
    pub(crate) gcobj: GCObjectPtr,
    pub(crate) string: Box<String>,
}

#[derive(Debug, Copy, Clone)]
pub(crate) struct InternedStrKey {
    pub(crate) ptr: NonNull<str>,
    pub(crate) gcobj: Option<GCObjectPtr>,
}

impl InternedStrKey {
    pub(crate) fn as_ref(&self) -> &str {
        unsafe { self.ptr.as_ref() }
    }
}

impl PartialEq for InternedStrKey {
    fn eq(&self, other: &Self) -> bool {
        self.as_ref() == other.as_ref()
    }

    fn ne(&self, other: &Self) -> bool {
        self.as_ref() != other.as_ref()
    }
}

impl Eq for InternedStrKey {}

impl Hash for InternedStrKey {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.as_ref().hash(state)
    }
}

pub(crate) trait ObjectAllocator {
    fn allocate_string(&mut self, str: &str) -> GCObjectPtr;

    fn allocate_function(&mut self, name: Option<GCObjectPtr>) -> GCObjectPtr;

    fn push_value(&mut self, v: Value);

    fn pop_value(&mut self);
}
