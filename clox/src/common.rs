use crate::value::*;

#[derive(Debug, Copy, Clone, Eq, PartialEq, FromPrimitive)]
pub enum OpCode {
    Constant = 0,
    ConstantLong,
    Nil,
    True,
    False,
    Pop,
    GetLocal,
    SetLocal,
    GetGlobal,
    GetUpValue,
    SetUpValue,
    DefineGlobal,
    SetGlobal,
    // != , <= and >= are constructed based on negating one of = < > instructions
    // Note: Is a <= b always the same as !(a > b)? According to IEEE 754, all comparison operators
    // return false when an operand is NaN. That means NaN <= 1 is false and NaN > 1 is also false.
    // But our desugaring assumes the latter is always the negation of the former.
    Equal,
    Greater,
    Less,
    Add,
    Subtract,
    Multiply,
    Divide,
    Not,
    Negate,
    Print,
    Jump,
    JumpIfFalse,
    Loop,
    Call,
    Closure,
    CloseUpValue,
    Return,
}

#[derive(Debug, Default)]
pub struct Chunk {
    bytes: Vec<u8>,
    constants: ValueArray,
    lines: Vec<usize>,
}

impl Chunk {
    pub fn new() -> Self {
        Self {
            bytes: Vec::with_capacity(8),
            constants: vec![],
            //TODO: Use run length encoding to store lines
            lines: Vec::with_capacity(8),
        }
    }

    pub fn get_opcodes(&self) -> &[u8] {
        self.bytes.as_slice()
    }

    pub fn write(&mut self, code: OpCode, line: usize) {
        self.bytes.push(code as u8);
        self.lines.push(line);
    }

    pub fn write_byte(&mut self, byte: u8, line: usize) {
        self.bytes.push(byte);
        self.lines.push(line);
    }

    pub fn add_constant(&mut self, value: Value) -> u32 {
        self.constants.push(value);
        ((self.constants.len() - 1) as u32) & CONSTANT_LONG_MASK
    }

    pub fn write_constant(&mut self, value: ConstantIndex, line: usize) {
        debug_assert!(value <= CONSTANT_LONG_MAX_INDEX_VALUE);
        if value <= CONSTANT_SHORT_MAX_INDEX_VALUE as u32 {
            self.write(OpCode::Constant, line);
            self.write_byte(value as u8, line);
        } else {
            self.write(OpCode::ConstantLong, line);
            let bytes = value.to_ne_bytes();
            self.write_byte(bytes[0], line);
            self.write_byte(bytes[1], line);
            self.write_byte(bytes[2], line);
        }
    }

    pub fn get_constants(&self) -> &[Value] {
        self.constants.as_slice()
    }

    pub(crate) fn get_constants_mut(&mut self) -> &mut [Value] {
        self.constants.as_mut_slice()
    }

    pub fn get_lines(&self) -> &[usize] {
        self.lines.as_slice()
    }

    pub fn clear(&mut self) {
        self.bytes.clear();
        self.lines.clear();
        self.constants.clear();
    }

    pub fn patch_jump(&mut self, chunck_offset: usize, jump: u16) {
        self.bytes[chunck_offset] = ((jump >> 8) & 0xFF) as u8;
        self.bytes[chunck_offset + 1] = (jump & 0xFF) as u8;
    }
}

#[cfg(test)]
mod test {
    use crate::common::OpCode;

    #[test]
    fn check_enum_size() {
        // ensure our enum size is actually 1 byte
        assert_eq!(std::mem::size_of::<OpCode>(), 1);
    }
}
