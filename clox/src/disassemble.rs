use crate::common::{Chunk, OpCode};
use crate::value::{print_value, ObjValue, Value, CONSTANT_LONG_MAX_INDEX_VALUE};
use num_traits::FromPrimitive;
use std::ops::Deref;

pub fn disassemble_chunk(chunk: &Chunk, name: &str) {
    println!("== {} ==", name);
    let mut offset: usize = 0;
    let opcodes_len = chunk.get_opcodes().len();
    while offset < opcodes_len {
        offset = disassemble_instruction(chunk, offset);
    }
}

fn simple_instruction(name: &str, offset: usize) -> usize {
    println!("{:16}", name);
    offset + 1
}

fn constant_instruction(name: &str, chunk: &Chunk, offset: usize) -> usize {
    let constant_index = chunk.get_opcodes()[offset + 1];
    print!("{:16} {:04} ", name, constant_index,);
    print_value(&chunk.get_constants()[constant_index as usize]);
    println!();
    offset + 2
}

fn constant_long_instruction(name: &str, chunk: &Chunk, offset: usize) -> usize {
    let bytes: [u8; 4] = [
        chunk.get_opcodes()[offset + 1],
        chunk.get_opcodes()[offset + 2],
        chunk.get_opcodes()[offset + 3],
        0,
    ];
    let constant_index = u32::from_ne_bytes(bytes);
    debug_assert!(constant_index <= CONSTANT_LONG_MAX_INDEX_VALUE);
    debug_assert!((constant_index as usize) < chunk.get_constants().len());
    println!("{:16} {:04} ", name, constant_index,);
    print_value(&chunk.get_constants()[constant_index as usize]);
    println!();
    offset + 4
}

fn byte_instruction(name: &str, chunk: &Chunk, offset: usize) -> usize {
    let slot = chunk.get_opcodes()[offset + 1];
    println!("{:16} {:04} ", name, slot);
    offset + 2
}

fn jump_instruction(name: &str, sign: isize, chunk: &Chunk, offset: usize) -> usize {
    let opcodes = chunk.get_opcodes();
    let jump = (opcodes[offset + 1] as u16) << 8 | opcodes[offset + 2] as u16;
    println!(
        "{:16} {:04} -> {:04}",
        name,
        offset,
        offset as isize + 3_isize + sign * jump as isize
    );
    offset + 3
}

pub fn disassemble_instruction(chunk: &Chunk, offset: usize) -> usize {
    print!("{:04} ", offset);
    let lines = chunk.get_lines();
    if offset > 0 && lines[offset] == lines[offset - 1] {
        print!("   | ")
    } else {
        print!("{:04} ", lines[offset]);
    }
    let byte = chunk.get_opcodes()[offset];
    if let Some(instruction) = FromPrimitive::from_u8(byte) {
        match instruction {
            OpCode::Return => simple_instruction("RETURN", offset),
            OpCode::Constant => constant_instruction("CONSTANTS", chunk, offset),
            OpCode::ConstantLong => constant_long_instruction("CONSTANTL", chunk, offset),
            OpCode::Negate => simple_instruction("NEGATE", offset),
            OpCode::Add => simple_instruction("ADD", offset),
            OpCode::Subtract => simple_instruction("SUBTRACT", offset),
            OpCode::Multiply => simple_instruction("MULTIPLY", offset),
            OpCode::Divide => simple_instruction("DIVIDE", offset),
            OpCode::Nil => simple_instruction("NIL", offset),
            OpCode::True => simple_instruction("TRUE", offset),
            OpCode::False => simple_instruction("FALSE", offset),
            OpCode::Not => simple_instruction("NOT", offset),
            OpCode::Equal => simple_instruction("EQUAL", offset),
            OpCode::Less => simple_instruction("LESS", offset),
            OpCode::Greater => simple_instruction("GREATER", offset),
            OpCode::Print => simple_instruction("PRINT", offset),
            OpCode::Pop => simple_instruction("POP", offset),
            OpCode::DefineGlobal => constant_instruction("DEFINE_GLOBAL", chunk, offset),
            OpCode::GetGlobal => constant_instruction("GET_GLOBAL", chunk, offset),
            OpCode::SetGlobal => constant_instruction("SET_GLOBAL", chunk, offset),
            OpCode::GetLocal => byte_instruction("GET_LOCAL", chunk, offset),
            OpCode::SetLocal => byte_instruction("SET_LOCAL", chunk, offset),
            OpCode::Jump => jump_instruction("JUMP", 1, chunk, offset),
            OpCode::JumpIfFalse => jump_instruction("JUMP_IF_FALSE", 1, chunk, offset),
            OpCode::Loop => jump_instruction("LOOP", -1, chunk, offset),
            OpCode::Call => byte_instruction("CALL", chunk, offset),
            OpCode::Closure => {
                let constant_index = chunk.get_opcodes()[offset + 1];
                print!("{:16} {:04} ", "CLOSURE", constant_index);
                let closure = &chunk.get_constants()[constant_index as usize];
                print_value(closure);
                println!();
                let mut local_offset = offset + 2;
                if let Value::Object(obj) = closure {
                    if let ObjValue::Function(function) = &obj.deref().object {
                        for _ in 0..function.upvalue_count as usize {
                            let is_local = chunk.get_opcodes()[local_offset];
                            local_offset += 1;
                            let index = chunk.get_opcodes()[local_offset];
                            local_offset += 1;
                            println!(
                                "{:04}      |                     {} {}",
                                local_offset - 2,
                                if is_local == 1 { "local" } else { "upvalue" },
                                index
                            );
                        }
                        local_offset
                    } else {
                        panic!("Expected function object");
                    }
                } else {
                    offset + 2
                }
            }
            OpCode::SetUpValue => byte_instruction("SET_UPVALUE", chunk, offset),
            OpCode::GetUpValue => byte_instruction("GET_UPVALUE", chunk, offset),
            OpCode::CloseUpValue => simple_instruction("CLOSE_UPVALUE", offset),
        }
    } else {
        println!("Unknown opcode {:04}", byte);
        offset + 1
    }
}
